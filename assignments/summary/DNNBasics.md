Deep Neural Network(DNN)

What is DNN?

A DNN is a network with multiple layers of interconnected neurons between the input and output layers. These in between layers are known as hidden layers. 

Neuron:
Thing that holds a number ranging from 0 to 1 known as its activation. Neuron is lit up as activation is increased.
Neuron: 
Low Activation Neuron:
High Activation Neuron: 

Why layers?
Layers of the network takes input from previous layers and helps in calculating the probability of the output.
What parameters should exist in a DNN?
It consist the weights for each activation of input neurons connected to a neuron in next layer and a bias which is added to weighed sum. This is done for making the network able to find the desired pattern.

Learning is finding the right weights and biases.
Sigmoid Function :  Function whose output range is between 0 to 1.
Graph showing behavior of sigmoid function : 
ReLU : It stands for Rectified Linear Unit which gives 0 for negative and acts as identity function for positive values.
Results in faster DNNs.
Graph showing behavior of ReLU : 
Cost Function : Used to determine how bad is the network. Needs to be minimised for a better DNN.

Gradient Descent : Process of repeatedly nudging an input of a function by some multiple of negative gradient is called
gradient descent. Used to find one of the local minimas to minimise output of cost function.

Back Propogation : It is an algorithm for fine-tuning the weights and biases in previous layer of a DNN by looking at the
output in the current layer and recursively applying the process to decrease the cost of DNN.

Stochastic Gradient Descent : It's a technique in which the large training data is divided into mini batches and gradient
descent of those mini batches is found using backprop iteratively. It is computationally
faster than finding the gradient of whole training data at a time.
