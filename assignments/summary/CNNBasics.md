CNN : Convolution Neual Network
CNN is a class od Deep Neural Network(DNN), also known as ConvNet basically used for classification.
Areas used: images recognition, images classifications, Objects detections, recognition faces etc.


Input : image into convolution layer


Choose parameters, apply filters(matrix) with strides, padding if requires. Perform convolution on the image and apply ReLU activation to the matrix.




Strides: Stride is the number of pixels shifts over the input matrix.


Kernels: Concatenation of filters.


Padding: If filter doesn't fit properly, padding takes place which are of two types.
 - Zero Padding:pad with enough zeros till it fits.
 - Valid Padding:keeping only the valid part, & drop the remaining where filter doesn't fit.


ReLU: Introduces non-linearity(to learn non-negative liner values).






Perform pooling to reduce dimensionality, reduces the number of parameters when the images are too large.

Spatial pooling reduces the dimensionality of each map but retains important information, having further types:
   -Max Pooling
   -Average Pooling
   -Sum Pooling

Pooling: 

(Max pooling takes the largest element from the rectified feature map. Taking the largest element could also take the average pooling. Sum of all elements in the feature map call as sum pooling.)


Add as many convolutional layers until satisfied.


Flatten the output and feed into a fully connected layer (FC Layer).Flattening layer is used to bring all feature maps matrices into a single vector.


Output the class using an activation function (Logistic Regression with cost functions) and classifies images.
Softmax is used at the output layer for classification.

